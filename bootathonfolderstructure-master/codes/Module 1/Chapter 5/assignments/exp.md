# P-N Jn DIODE  CHARACTERISTICS

**Aim** : 1. To plot Volt-Ampere Characteristics of Silicon P-N Junction Diode.  
2. To find cut-in Voltage for Silicon P-N Junction diode.  
3. To find static and dynamic resistances in both forward and reverse biased conditions
for Si P-N Junction diode.

# Theory : 

Donor impurities (pentavalent) are introduced into one-side and acceptor impurities
into the other side of a single crystal of an intrinsic semiconductor to form a p-n diode with a
Junction called depletion region (this region is depleted off the charge carriers). This Region
gives rise to a potential barrier V called Cut- in Voltage. This is the voltage across the diode
at which it starts conducting. It can conduct beyond this Potential.

The P-N junction supports uni-directional current flow. If +ve terminal of the input supply is
connected to anode (P-side) and –ve terminal of the input supply is connected to cathode
(N- side) then diode is said to be forward biased. In this condition the height of the potential
barrier at the junction is lowered by an amount equal to given forward biasing voltage. Both
the holes from p-side and electrons from n-side cross the junction simultaneously and
constitute a forward current (injected minority current – due to holes crossing the junction
and entering N-side of the diode, due to electrons crossing the junction and entering P-side of
the diode). Assuming current flowing through the diode to be very large, the diode can be
approximated as short-circuited switch. 

**Circuit Diagram**  

For Forward Bias
![exp1](exp1.jpg)



For Reverse Bias
![exp2](exp2.jpg)


# Procedure:
**Forward Biased Condition** :
1. Connect the circuit as shown in figure (1) using silicon PN Junction diode.
2. Vary Vf gradually in steps of 0.1 volts upto 5volts and note down the corresponding
readings of If .
3. Step Size is not fixed because of non linear curve and vary the X-axis variable (i.e. if
output variation is more, decrease input step size and vice versa).
4. Tabulate different forward currents obtained for different forward voltages.

**Reverse biased condition**  :
1. Connect the circuit as shown in figure (2) using silicon PN Junction diode.
2. Vary Vr gradually in steps of 0.5 volts upto 8 volts and note down the corresponding
readings of Ir.
3. Tabulate different reverse currents obtained for different reverse voltages. (Ir = VR /
R, where VR is the Voltage across 10K)

# Precautions:
1. While doing the experiment do not exceed the ratings of the diode. This may lead to
damage the diode.
2. Connect voltmeter and Ammeter in correct polarities as shown in the circuit diagram.
3. Do not switch ON the power supply unless you have checked the circuit connections
as per the circuit diagram.